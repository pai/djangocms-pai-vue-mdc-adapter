from django.apps import AppConfig


class DjangocmsPaiVueMdcAdapterConfig(AppConfig):
    name = 'djangocms_pai_vue_mdc_adapter'
